\documentclass{article}

\makeatletter
\renewcommand\paragraph{\@startsection{paragraph}{4}{\z@}%
% display heading, like subsubsection
                                     {-3.25ex\@plus -1ex \@minus -.2ex}%
                                     {1.5ex \@plus .2ex}%
                                     {\normalfont\normalsize\bfseries}}
 \setcounter{secnumdepth}{3}
\makeatother


\usepackage[utf8]{inputenc}
\usepackage{comment} % multi-line comment
\usepackage[]{todonotes} 
% \usepackage[disable]{todonotes} 

\usepackage{geometry} % less bordering white space
    % normal:
    \geometry{a4paper, top=25mm, left=20mm, right=20mm, bottom=40mm, headsep=10mm, footskip=20mm}
    % for todonotes:
    \geometry{a4paper, top=25mm, left=10mm, right=30mm, bottom=40mm, headsep=10mm, footskip=20mm}


\newcommand{\capwidth}{0.9\columnwidth} % used for setting the width of captions
\usepackage{enumitem,amssymb} % for todo lists
% https://tex.stackexchange.com/questions/247681/how-to-create-checkbox-todo-list

\usepackage[ % Für Benutzung von Citation-kürzeln wie natbib
    authordate,
    bibencoding=auto,
    strict,
    backend=biber,
    maxcitenames=2,
    natbib
    ]{biblatex-chicago}
\addbibresource{bib.bib}

% make colored hyperref links for citations
\usepackage{hyperref}
\usepackage{xcolor}
\hypersetup{
    colorlinks,
    linkcolor={black!50!black},
    citecolor={blue!50!black},
    urlcolor={blue!80!black}
}

% keine schusterjungen, plz :sob:
% https://tex.stackexchange.com/questions/4152/how-do-i-prevent-widow-orphan-lines
\widowpenalty10000
\clubpenalty10000

%\title{Advisory Panel Meeting on 12.04.2022 \\ Status Report and Outline}
%\author{Hernan Campos}
%\date{October 2021}

\begin{document}

% to make counter match model
\setcounter{section}{5}
\setcounter{subsection}{1}

\subsection{CLICCS A2, Clouds and Tropical Circulation}

Applicants: Ann Kristin Naumann (MPI-M); 
%Applicants: Hernan Campos (MPI-M), Ann Kristin Naumann (MPI-M); 
%CLICCS project chairs: Stefan Bühler (UHH), Bjorn Stevens (MPI-M); 
Continuation application DKRZ project: bm1183

\subsubsection{Project Overview}

The tropics are the engine of the atmospheric general circulation, and their response to warming helps set Earth’s climate sensitivity. Recently it has become appreciated that shallow circulations in the lower troposphere are crucial for linking fluxes of energy – at the surface and through the atmosphere – and the deep overturning circulations that dictate the distribution of precipitation \citep{bony2015cloudscirculation}. Changes in the tropical circulation, be it long-term shifts in tropical convection, such as seen in the Sahel, or changes in the strength of the large-scale Walker circulation across the Pacific \citep{plesca2018fast}, have profound consequences for ecosystems and societies, not just in the tropics. A central question for the coming years will be the extent to which the tropics warm, and how the circulation systems respond to this warming. Recent work has highlighted the role of radiative effects (often, but not always, from clouds) in mediating processes as diverse as convective self-aggregation \citep{naumann2017conceptual,naumann2019moist}, ENSO \citep{radel2016amplification}, decadal variability \citep{bellomo2015influence}, and the structure of the ITCZ (e.g., \cite{shaw2015tug}). How these drivers of circulation will change with warming, is poorly known. 

There are three reasons for addressing this question now: (1) Recent advances in computing power make it now feasible to run circulation models on large domains with resolved convection, which hitherto has been impossible, hindering progress. (2) New satellite missions will be launched in the coming years (EarthCARE, ADM-Aeolus, Metop SG), which will provide wholly new observations on clouds and the circulation. (3) The PIs lead international experimental initiatives to better understand controls on cloudiness over the tropical Atlantic \citep{bony2017eurec4a}, which this project can leverage. 
The project aims to better understand the tropical heat budget, its link to circulation systems, and how these respond to warming. The understanding of the systems sensitivity will be crucial in reducing the epistemic uncertainty in this globally important region – a prerequisite for more tightly constraining global climate sensitivity and hence the range of possible and plausible climate futures. This directly contributes to answering the question of which climate futures are possible and which are plausible, the overarching research question of CLICCS. 


\subsubsection{Range of Planned Work from the Scientific View}

Cloud feedback remains a major source of uncertainty in our attempt to accurately estimate climate sensitivity \citep{bretherton2015insights,ipcc2021}. Here tropical low clouds, shallow cumulus, are of particular importance \citep{gettelman2016processes}. Their small size calls for very high resolution, when modelling their effects. At the same time large scale changes in circulation and storm tracks could influence and structure shallow cumulus clouds.  %\citep{karlsson2008cloud}. 
Investigating both small and large scales in the same simulation is difficult, because computational resources put a limit to the model setup. Past studies have either been based on coarse large scale simulations \citep{nam2012cmipCRE} or they have turned to idealized small scale setups \citep{radtke2020shallow}. We want to get the best of both worlds by using a realistic small scale setup that is forced by input from a large scale simulation. We will use this setup for a small set of experiments, exploring the effects of different aspects of global warming on shallow cumulus clouds and how they influence the cloud radiative effect.

%Clouds affect the global top of the atmosphere energy budget by reflecting shortwave radiation entering the system as well as reflecting back longwave radiation emitted by the surface. By comparing cloudy and clear sky, the net cloud radiative effect on climate (CRE) can be determined. This CRE has been a major contributor to uncertainties in climate sensitivity estimates with large differences in CRE estimation between different models \citep{randall2007_ipcc_ar4}. Since the IPCC AR4 report, efforts have been made tackle the problem following a \textit{divide et impera} strategy, looking at the CRE of specific processes (reviewed in \cite{bretherton2015insights}), specific cloud types \citep{hartmann1992effect} or specific regions \citep{gettelman2016processes}. Weighted regional feedbacks have then been aggregated to estimate the global CRE \citep{forster2021_ipcc_ar6}. Low clouds in the trades are of particular interest because they are highly reflective and contribute to the shortwave feedback \citep{bretherton2015insights}.

The technical foundation for our work is a realistic large eddy simulation (LES) setup that was designed to accompany the EUREC4A field campaign \citep{schulz2021AGUabstract}.
The high resolution EUREC4A LES simulations have shown to reproduce the fine grained clouds and their mesoscale cloud patterns during the period of the field campaign, matching observations fairly well \citep{schulz2021thesis}.
The computing domains cover the area of the field campaign and are located east of Barbados, between 7.5 and 17 \textdegree{}N and 45 and 60 \textdegree{}W. 
%One of the main goals of the field campaign itself was to get a better understanding of trade wind clouds and the, the processes that control them and the environment they are formed in \citep{bony2017eurec4a}. The high resolution EUREC4A LES simulations have shown to reproduce the fine grained clouds and their mesoscale cloud patterns during this period, matching observations fairly well \citep{schulz2021thesis}. We will build upon this experiment setup and we will introduce perturbations that reflect different aspects of global warming. The simulations are forced by prescribed boundary conditions for the edges of the computing domain. The sea surface, the lateral boundaries and its initial field are prescribed. 

%\citet{stevens2020sugar} have identified four classes of meso scale cloud patterns from their visual appearance in satellite images and named them \textit{sugar}, \textit{gravel}, \textit{fish} and \textit{flowers}. The variables we want to explore are the ones  \citet{bony2020sugar} and \citet{schulz2021characterization} have linked to specific patterns of mesoscale organisation.  \citet{bony2020sugar} distinguish mesocale patterns via atmospheric stability and surface wind speed and assign CRE ranges to each pattern, via their cloud amount. \citet{schulz2021characterization} find a correlation of pattern with wind speed and additionally find discriminating factors for single patterns: a high sea surface temperature (SST) for \textit{Sugar}, high subsidence for \textit{Gravel}, high stability for \textit{Flowers} and strong convergence for \textit{Flowers}. \citet{bony2020sugar}, finding no correlation of mesoscale patterns with SST. They propose an expected increase in atmospheric stability with global warming and thus predict more frequent \textit{Fish} and/or \textit{Flower}, which would result in a larger cloud fraction, more reflection of incoming shortwave radiation and a more negative CRE.

A climate change forcing signal that represents global warming will be derived by comparing different scenarios of General Circulation Models (GCM). By calculating the difference in the 3D output of a GCM representing a control scenario from a GCM representing a global warming scenario a \textit{Delta} can be arrived. This Delta represents the isolated effect of global warming and can be applied to our boundary conditions either for single variables or for sets of different variables. This method -- referred to as \textit{pseudo global warming} -- has successfully been applied to a variety of regional models to derive site specific climate change predictions \citep{kroner2017separating, li2019high}. 
%We will be able to directly compare the \textit{historical} EUREC4A simulation and its abundantly available measuring data to our global warming version of the same time frame. As we aim at understanding the involved processes, we will change single variables as isolated as possible. The experiments we will conduct can be summarized as:
\citet{bony2020sugar} and \citet{schulz2021characterization} have correlated different proxies to specific patterns of mesoscale organisation. In their studies sea surface temperature and winds play an important role as discriminators for mesoscale patterns. Additionally we will conduct an experiment to investigate the relative humidity feedback (e.g. \cite{allan2022global}) and an experiment with a \textit{complete} set of Deltas.




\subsubsection{Numerical Methods and Solution Procedures}


\paragraph{Mathematical and/or computational aspects}

The simulation is run in two nested domains with ca. 600 and 300\,m resolution (equivalent size square edge length). High resolutions are needed to accurately represent shallow cumulus cloud cover (see below)

\paragraph{Algorithmic/mathematical/numerical methods and solution procedures} 

Our experiment setup uses an atmosphere only version of the ICON model in its LES configuration with NWP physics (developed for numerical weather prediction) and computationally intensive two moment microphysics. 

\paragraph{Particular suitability to solve the problem with help of HLRE-4}

The high resolution model needs a lot of computational resources. HLRE-4 offers these resources. Compared to HLRE-3, HLRE-4 is more efficient, offering more CPUs per node.

\paragraph{Performance benefits depending on the number of used CPUs (scalability) }

The ICON-AES version has been shown to scale well over thousands of processors and on various platforms among others through the HDCP2 and DYAMOND projects.

\subsubsection{Required Computing Time and Amount of Storage Space}

As we aim to look at effects of shallow cumulus clouds, our model resolution has to fit this intent. Prior studies suggest that cell sizes of below 500m (edge length) are most apt to represent these small scale clouds \citep{radtke2020shallow}. The two domains we use have 4.5 and 11.8 million computational cells respectively, with 150 vertical levels. The ICON-LES runs nested domains simultaneously. 

\begin{comment}
import xarray as xr
import numpy as np    

def eureca_grid(domain='DOM01'):
    return xr.open_zarr(f"https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/grids/EUREC4A_PR1250m_{domain}.zarr")

for dom in ['DOM01','DOM02']:
    d = eureca_grid(domain=dom)
    print(dom)
    for dim in ['clon','clat']:
        print(dim, ';', np.rad2deg(np.nanmin(d[dim].values)), np.rad2deg(np.nanmax(d[dim].values)) )
    print('ncells', ':', len(d.clat.values))
\end{comment}

We propose four experiments with different perturbations. The simulation setup is the same for all four experiments. This ensures comparability among the experiments as well as to the original EUREC4A LES simulation, that will serve as a control reference. Our experiment domain is forced via its boundaries in space and time: initial field, sea surface and lateral edges. Only the boundary conditions are changed according to four scenarios that reflect different aspects of climate change under global warming conditions. These different boundary conditions will not be stored as files. Instead a script to create these input files will assure reproducibility. 

In many aspects of the experiment design we follow the original simulation. The length of our experiments will aim at the same 40 day period, which matches the duration of the filed campaign. Additionally this time frame has shown to contain examples of all four mesoscale patterns \citep{schulz2021thesis}.

This project is embedded in the joint effort of several research groups, trying to complement the EUREC4A field campaign with model data (e.g. \cite{jansson2022representing}). These research groups have agreed upon a joint output format to facilitate inter comparison under the name \textit{EUREC$^4$A-CONSTRAIN model intercomparison}. This includes 3-hourly 3D output for a set of 8 variables, as well as 2D and 1D (meteogram) output in higher frequency (10 minutes, 14 variables) to allow analysis on shorter time scales.  

Each run will need approximately 3000 node hours and will produce roughly 25\,TiBs of output data (including restart files). 
%Experiences with compression of past projects suggest that this output can be compressed almost tenfold without considerable loss of information \citep{klower2021compressing}. 
The compressed and postprocessed data will be made available for a broader user group with the help of an intake catalog (e.g. \cite{banihirwe2020intake}). This will mean 100\,TiB of disk space occupied by simulation output for the archive project, with a potential peak disk usage of 125\,TiB (3 postprocessed experiments + 1 raw simulation output).




\begin{table}[h!]
  \begin{center}
    \caption{Estimated resources needed.}

\begin{tabular}{ | l | p{0.1\textwidth} | p{0.1\textwidth} | p{0.1\textwidth} | p{0.1\textwidth} | p{0.1\textwidth} | p{0.1\textwidth} | }
    \hline
    \textbf{Name} & \textbf{Model resolution} & \textbf{Simulated years} & \textbf{Levante CPU [Node hours]} & \textbf{Levante storage [TiB]} & \textbf{Archive project [TiB]} & \textbf{Archive long term [TiB]} \\ \hline
    EUREC4A\_SST & 600m, 300m & 40 days = 0.110 years & 3000 & 50 & 25 & 10 \\
     \hline
    EUREC4A\_wind & 600m, 300m & 40 days = 0.110 years & 3000 & 50 & 25 & 10 \\
     \hline
    EUREC4A\_relhum & 600m, 300m & 40 days = 0.110 years & 3000 & 50 & 25 & 10 \\
     \hline
    EUREC4A\_complete & 600m, 300m & 40 days = 0.110 years & 3000 & 50 & 25 & 10 \\
     \hline
\end{tabular}
% the conservation times are:
% Levante storage   : temporary (depends)
% Archive project   :  1y
% Archive long term : 10y

  \end{center}
\end{table}

\begin{comment}
[m300872@levante6 EUREC4A]
cd /work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/experiments/EUREC4A
du ./
    493G	./multifile_restart_atm_20200218T220000Z.mfr
    479G	./multifile_restart_atm_20200201T220000Z.mfr
    521G	./multifile_restart_atm_20200219T100000Z.mfr
    488G	./multifile_restart_atm_20200210T220000Z.mfr
    487G	./multifile_restart_atm_20200207T220000Z.mfr
    38M	    ./EUREC4A_constants_DOM01.zarr
    14T	    ./3D
    205G	./latlon
    34G	    ./meteograms_lev0
    316M	./pamtra
    145T	./radiation
    1.4T	./reff
    2.4T	./RTTOV
    11T	    ./surface
    175T	./
    
surface:
frequency: 10min
dom01 : 11810 files * 0.26GB
dom02 :  5797 files * 1.40GB
variables:
		u_10m:long_name = "zonal wind in 10m" ;
		v_10m:long_name = "meridional wind in 10m" ;
		rh_2m:long_name = "relative humidity in 2m" ;
		t_2m:long_name = "temperature in 2m" ;
		qv_2m:long_name = "specific water vapor content in 2m" ;
		t_seasfc:long_name = "sea surface temperature" ;
		shfl_s:long_name = "surface sensible heat flux" ;
		lhfl_s:long_name = "surface latent heat flux" ;
		tqv_dia:long_name = "total column integrated water vapour (diagnostic)" ;
		tqc_dia:long_name = "total column integrated cloud water (diagnostic)" ;
		tqi_dia:long_name = "total column integrated cloud ice (diagnostic)" ;
		rain_gsp_rate:long_name = "gridscale rain rate" ;
		tot_prec:long_name = "total precip" ;
		clct:long_name = "total cloud cover" ;
		pres_sfc:long_name = "surface pressure" ;
3D
frequency: 60min
dom01 : 328 files * 1.2GB
dom02 : 320 files * 0.3GB
variables:
		u:long_name = "Zonal wind" ;
		v:long_name = "Meridional wind" ;
		w:long_name = "Vertical velocity" ;
		temp:long_name = "Temperature" ;
		pres:long_name = "Pressure" ;
		theta_v:long_name = "virtual potential temperature" ;
		rho:long_name = "density" ;
		qv:long_name = "Specific humidity" ;
		qc:long_name = "specific cloud water content" ;
		qr:long_name = "rain mixing ratio" ;
		cloud_num:long_name = "cloud droplet number concentration" ; (not 3D)
		
radiation:
frequency: 10min
dom01 : 1006 files *  42GB
dom02 :  977 files * 109GB
variables:
		sou_t:long_name = "shortwave upward flux at TOA" ;  (not 3D)
		sob_t:long_name = "shortwave net flux at TOA" ; (not 3D)
		sod_t:long_name = "downward shortwave flux at TOA" ; (not 3D)
		thb_t:long_name = "thermal net flux at TOA" ; (not 3D)
		ddt_temp_radsw:long_name = "short wave radiative temperature tendency" ;
		ddt_temp_radlw:long_name = "long wave radiative temperature tendency" ;
		lwflx_dn_clr:long_name = "longwave downward clear-sky flux" ;
		lwflx_dn:long_name = "longwave downward flux" ;
		lwflx_up_clr:long_name = "longwave upward clear-sky flux" ;
		lwflx_up:long_name = "longwave upward flux" ;
\end{comment}


\subsubsection{Additional Value Compared to Other Projects}

We will have access to additional resources from the \textit{Drivers of tropical circulation} project (mh1126). These will be used for debugging and test runs, as well as for analysis. However, mh1126 does not provide enough resources to cover the production runs described in this proposal.

\subsection*{ICON-NWP}
The german weather service DWD developed a physics module for ICON that is optimised for numerical weather prediction (NWP). Besides horizontal and vertical transport processes in the atmosphere, diabatic processes like radiation, turbulence, formation of clouds, and precipitation play a major role for NWP. We use this physics implementation in its original uncoupled form with prescribed sea surface temperatures and in a limited domain.



\newpage



\clearpage
\printbibliography



\end{document}
