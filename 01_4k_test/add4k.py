#!/sw/spack-levante/mambaforge-4.11.0-0-Linux-x86_64-sobz6z/bin/python

#SBATCH --partition=compute
#SBATCH --account=mh0926
#SBATCH --nodes=1
#SBATCH --ntasks-per-node=24
#SBATCH --time=04:00:00
#SBATCH --mail-type=ALL
#SBATCH --mail-user=hernan.campos@mpimet.mpg.de
#SBATCH --output=%j_%x.log
#SBATCH --error=%j_%x.log

import os
import sys
import glob
import subprocess as sub


import xarray as xr
import random
from numpy import array_split as list_split


def get_random_hash():
    hash = random.getrandbits(128)
    return str(hex(hash))

def basename(path):
    # should yield the same result as the bash command
    return  path.split('/')[-1].split('.')[0]

def temp_ident():
    return '_temporary_'

def set_pahts():
    # sets paths and variable names globally
    global source_path, target_path, temp_path
    global sst_varname, sst_dir, sst_files
    global ini_varname, ini_dir, ini_files
    global lat_varname, lat_dir, lat_files

    # where data comes from, where it goes
    source_path = '/work/mh0010/m300408/DVC-test/EUREC4A-ICON/EUREC4A/'
    target_path = '/work/mh0926/m300872/les_plus4_forcing/'
    temp_path   = '/work/mh0926/m300872/temp_' + get_random_hash()

    # sst_sic, sea surface temperature and sea ice
    sst_varname = 'SST'
    sst_dir       = 'sst_sic/data/'
    sst_filenames = ['sst_sic.nc', 'sst_sic_DOM01.nc', 'sst_sic_DOM02.nc', 'sst_sic_DOM03.nc']
    sst_files = [os.path.join(sst_dir, f) for f in sst_filenames]

    # initc, initial 3D field
    ini_varname = 't'
    ini_dir   = 'initc/20200109/'
    ini_file  = 'initc_EUREC4A_PR1250m_DOM01_2020010910.nc'
    ini_files = [ os.path.join(ini_dir, ini_file) ]

    # latbc, lateral boundary conditions
    lat_varname  = 'temp'
    lat_parent_dir  = 'latbc/'
    lat_dir   = [os.path.join(lat_parent_dir, dirname) for dirname in os.listdir(os.path.join(source_path, lat_parent_dir))
                   if os.path.isdir(os.path.join(source_path, lat_parent_dir, dirname))] # a list of all subdirectories
    lat_files = [] # a list of all files in these subdirectories
    for d in lat_dir:
        for e in os.listdir(os.path.join(source_path, d)):
            lat_files.append(os.path.join(d,e))

def build_directory_structure():
    sub.run(['mkdir', '-p', temp_path])
    sub.run(['mkdir', '-p', os.path.join(target_path, sst_dir)])
    sub.run(['mkdir', '-p', os.path.join(target_path, ini_dir)])
    for d in lat_dir:
        sub.run(['mkdir', '-p', os.path.join(target_path, d)])


def cdo_add(f, value, remove_prognostic=False):
    # these are the steps needed to add to a single variable.
    # cdo can not add to a single variable, thus splitting and merging is needed
    cdo_split(f)
    if remove_prognostic: cdo_remove(f)
    cdo_addc(f, value)
    cdo_merge(f)

def cdo_split(f):
    # split netCDF file into many files, one variable in each
    ifile = os.path.join(source_path, f)
    ofile = os.path.join(temp_path, basename(f) + temp_ident())
    command = ['cdo', *cdo_options, 'splitname', ifile, ofile ]
    sub.run(command)

def cdo_addc(f, value):
    # add a constant to the file containing temperature
    ifile = os.path.join(temp_path, f"{basename(f)}{temp_ident()}{vardict[f]}.nc")
    ofile = os.path.join(temp_path, f"{basename(f)}{temp_ident()}{vardict[f]}_plus{value}.nc")
    command = ['cdo', *cdo_options, 'addc,'+str(value), ifile, ofile ]
    sub.run(command)

    # replace the file with unchanged temperature
    swap  = ifile
    ifile = ofile
    ofile = swap
    command = ['mv', ifile, ofile ]
    sub.run(command)

def cdo_merge(f):
    # join all files to a single file again.
    ifile = glob.glob(os.path.join(temp_path, f"{basename(f)}{temp_ident()}*.nc"))
# # begin db
#     print('cdo_merge ifile:')
#     for i in ifile:
#         if 'rho' in i or 'theta' in i:
#             print('KILL:',i)
#         else:
#             print(i)
# # end db
    ofile = os.path.join(target_path, f)
    command = ['cdo', *cdo_options, '-O', 'merge',  *ifile, ofile ]
    sub.run(command)

    # remove temporary files
    command = ['rm', *ifile]
    sub.run(command)

def cdo_remove(f):
    ifile = glob.glob(os.path.join(temp_path, f"{basename(f)}{temp_ident()}*.nc"))
    for i in ifile:
        if 'rho' in i or 'theta' in i:
            command = ['rm', i]
            sub.run(command)


def xr_add(f, value, remove_prognostic=False):
    global source_path, target_path, temp_path

    ifile = os.path.join(source_path, f)
    ofile = os.path.join(target_path, f)
    data = xr.open_dataset(ifile)
    vardict = get_vardict()
    data[vardict[f]] += value
    if remove_prognostic:
        rename_dict = {'rho':'DELETED-rho', 'theta_v':'DELETED-theta_v'}
        data = data.rename(rename_dict)
    ofile = '/home/m/m300872/test.nc'
    homecopy = '/home/m/m300872/test.nc'
    ofile = os.path.join(temp_path,basename(f)) + '.nc'
    print(list(data))
    print(ifile)
    print(ofile)
    data.to_netcdf(ofile)
#    command = ['rm', ofile]
#    print(command)
#    sub.run(command)
    command = ['cp', ofile, homecopy]
    print(command)
    sub.run(command)
    print('f',f)
    command = ['mv', ofile, os.path.join(target_path, f)]
    print(command)
    sub.run(command)


def get_vardict():
    global sst_varname, ini_varname, lat_varname
    global sst_files,   ini_files,   lat_files
    # and associate each file name with the corresponding variable name for temperature: 
    vardict = {}
    for f in sst_files:
        vardict[f] = sst_varname
    for f in ini_files:
        vardict[f] = ini_varname
    for f in lat_files:
        vardict[f] = lat_varname
    return vardict

def read_argv():
    global file_list
    if len(sys.argv) == 2:
        if sys.argv[1] == 'test' :
            # just uses one, for testing
            file_list = file_list[0:1]
        if sys.argv[1] == 'slice':
            # only perform on a part of the list, for parallelisation
            try:
                n, d = int(sys.argv[2]), int(sys.argv[3])
            except:
                print('usage: slice <int a> <int b>')
                print('will work on the a-th part of a subset if split into b equal parts')

        # use np.array_split (aka. list_split) to get a chunk of the list
        file_list = list(list_split(file_list, d)[n-1])

def add_to_var(f, value, remove_prognostic=False, backend='cdo'):
    print(f"add_to_var with {f},{value},{remove_prognostic}, {backend}")
    if   backend == 'cdo':
        cdo_add(f, value, remove_prognostic)
    elif backend == 'xarray':
        xr_add(f, value, remove_prognostic)
    else:
        raise ValueError(f"unknown backend defined: {backend}")



######  M A I N   #####
if __name__ == "__main__":

    added_value = 4 # how much is added
    cdo_options = ['-P', '8']

    set_pahts()
# begin db 
    # target_path = '/work/mh0926/m300872/remove_rho/'
# end db 
    build_directory_structure()
    vardict = get_vardict()
    file_list = sst_files + ini_files + lat_files
    read_argv()

# just work on the initc:
# begin db 
    file_list = ini_files
# end db 


    print(f"file list length = {len(file_list)}")
    for i,f in enumerate(file_list):
        print(f"{i}, filename: {f}")
        remove_prognostic = vardict[f] == ini_varname
        remove_prognostic = False
        # print('remove prognostic ', remove_prognostic)
        # cdo_add(f, remove_prognostic=remove_prognostic)
        add_to_var(f, added_value, backend='cdo')
#        add_to_var(f, added_value, remove_prognostic=remove_prognostic, backend='xarray')

    # remove tempory folder
    command = ['rm', '-r', temp_path]
    sub.run(command)


