import os
import numpy as np
import warnings
import hashlib
import eurec4a
import xarray as xr

def disable_warnings():
    warnings.filterwarnings('ignore')
    
def get_hash(string):
    # https://www.pythoncentral.io/hashing-strings-with-python/
    hash_object = hashlib.sha1(string.encode())
    return hash_object.hexdigest()

def basename(path, extension=True):
    if extension: 
        return os.path.basename(path)
    else: # removes extension
        return os.path.splitext(os.path.basename(path))[0]
    
def dirname(path):
    return os.path.dirname(path)

def extension(path):
    return os.path.splitext(path)[1]

def is_netcdf(f):
    return extension(f) == '.nc'

def get_filelist(lost='', conserved='./'):
    '''
    crawls the directory `lost_path`+`conserved_path` and 
    returns a list of files in all subdirectories. The returned 
    paths are relative paths to `lost_path`.
    '''
    file_list = []
    for root, directories,files in os.walk(os.path.join(lost, conserved)):
        for name in files: 
            if is_netcdf(name):
                path = os.path.join(os.path.relpath(root, start=lost), name)
                file_list.append(path)
    file_list.sort()
    return file_list

def n_elements(dataset):
    sizes = [f[v].size for v in list(f)]
    return np.sum(sizes)

def gb(byte):
    return byte / 1e9

def tb(byte):
    return byte / 1e12

def icontime2numpytime(timefloat):
    raise Warning('Use postprocessing.icon2datetime() instead')
    date_string = str(int(timefloat))
    h_ = 24 * (timefloat - int(timefloat))
    h  = int(h_)
    m_ = 60 * (h_ - h)
    m  = int(np.round(m_))
    if m == 60: m = 0; h += 1
    return np.datetime64(f'{date_string[0:4]}-{date_string[4:6]}-{date_string[6:8]}T{str(h).rjust(2,"0")}:{str(m).rjust(2,"0")}')

def create_directory_structure(file_list, out_directory):
    for f in file_list:
        dirpath = os.path.join(out_directory,dirname(f))
        os.makedirs(dirpath, exist_ok=True)
        
def sublist(inlist, n_lists, index):
    return list(np.array_split(inlist, n_lists)[index])

# a sound alarm to notify me, a cell has finished. i get distracted.
# https://gist.github.com/tamsanh/a658c1b29b8cba7d782a8b3aed685a24
from IPython.lib.display import Audio
def beep(fr=4410, sec=1.0/7):
    t = np.linspace(0, sec*2, int(fr*sec))
    audio_data = np.sin(2*np.pi*300*t) + np.sin(2*np.pi*240*t)
    return Audio(audio_data, rate=4410, autoplay=True)

class MinIntake:
    def __catalog__(self):
        return [s for s in dir(self) if not s.startswith('__')]
    def __getitem__(self, item):
        return self.__catalog__()[item]
    pass

def int_factorize(x, always_bigger=True):
    '''
    square root for integers
    '''
    m = int(np.sqrt(x))
    n = int(x/m)
    if m*n < x and always_bigger : m += 1
    return m,n

def match(string, keywords):
    '''
    wether 
    '''
    checklist = list()
    for k in keywords:
        checklist.append(k in string)
    return all(checklist)

def disk_data_from_keywords(directory, keywords, exclude=False):
    files = [f for f in os.listdir(directory) if match(f, keywords)]
    if exclude: 
        for x in exclude:
            files = [f for f in files if not x in files]
    files = [os.path.join(directory, f) for f in files]
    dataset = xr.open_mfdataset(files)
    return dataset

def eureca_data_from_keywords(keywords):
    cat = eurec4a.get_intake_catalog()['simulations']['ICON']['LES_CampaignDomain_control']
    identifier = [f for f in list(cat) if match(f, keywords)]
    if    len(identifier) > 1 : raise(ValueError(f'Keywords match more than one dataset: {identifier}'))
    elif  len(identifier) < 0 : raise(ValueError(f'No match for these keywords.'))
    else: return cat[identifier[0]].to_dask()