import numpy as np
import xarray as xr
import pandas as pd
import datetime
import eurec4a

def icon2datetime(icon_dates):
    """
    From: esm_analysis lib
    Convert datetime objects in icon format to python datetime objects.
    :param icon_dates: collection
    :return dates:  pd.DatetimeIndex
    icon_dates i.e
        time = icon2datetime([20011201.5])
    """
    try:
        icon_dates = icon_dates.values
    except AttributeError:
        pass

    try:
        icon_dates = icon_dates[:]
    except TypeError:
        icon_dates = np.array([icon_dates])

    def _convert(icon_date):
        frac_day, date = np.modf(icon_date)
        frac_day *= 60 ** 2 * 24
        date = str(int(date))
        date_str = datetime.datetime.strptime(date, '%Y%m%d')
        td = datetime.timedelta(seconds=int(frac_day.round(0)))
        return date_str + td

    conv = np.vectorize(_convert)
    try:
        out = conv(icon_dates)
    except TypeError:
        out = icon_dates
    if len(out) == 1:
        return pd.DatetimeIndex(out)[0]
    return pd.DatetimeIndex(out)

def overlap_timesteps(datasets):
    t = datasets[0].time.values
    for dataset in datasets[1::]:
        t = [x for x in t if x in dataset.time.values]
    return t

def time_overlap(datasets):
    t = overlap_timesteps(datasets)
    return tuple([d.sel(time=t) for d in datasets])


def clean_surface_data(dataset, 
                       rename_vars={'ncells':'cell'}, 
                       drop_dims={'height':10, 'height_2':2},
                       dim_order=['time', 'cell'],
                       convert_time=True):
    if rename_vars: dataset = dataset.rename(rename_vars)
    if drop_dims:   dataset = dataset.sel(drop_dims).drop_vars(list(drop_dims))
    if dim_order: dataset = dataset[dim_order + list(dataset)]
    if convert_time: dataset = dataset.assign_coords(time = icon2datetime(dataset.time.values))
    return dataset

def clean_volume_data(dataset, 
                      rename_vars={'ncells':'cell'}, 
                      dim_order=['time', 'cell', 'height', 'bnds'],
                      convert_time=True):
    if rename_vars: dataset = dataset.rename(rename_vars)
    dataset = dataset.drop("height_2").rename({"height_2":"height"})
    if dim_order: dataset = dataset[dim_order + list(dataset)]
    if convert_time: dataset = dataset.assign_coords(time = icon2datetime(dataset.time.values))
    return dataset


def eurec4a_grid(domain='DOM01', ncells=None):
    grids = eurec4a.get_intake_catalog().simulations.grids
    if ncells:
        if ncells == 4528560:
            return grids[list(grids)[0]].to_dask()
        if ncells == 11792076:
            return grids[list(grids)[1]].to_dask()
        else:
            raise ValueError("ncells could not be connected with a grid")
    else:
        if   domain == 'DOM01':
            return grids[list(grids)[0]].to_dask()
        elif domain == 'DOM02':
            return grids[list(grids)[1]].to_dask()
        else:
            raise ValueError("Unknown domain. Choose between 'DOM01' and 'DOM02'.")
        
def add_grid(d, clean=True, drop_vars=True):
    if clean: drop_vars=True
    grid = eurec4a_grid(ncells=len(d.cell))
    grid_vars = list(grid)
    d = xr.merge((grid,d))
    if drop_vars: 
        d = d.drop_vars(grid_vars)
    if clean:
        d = d.drop_vars(['elat','elon','vlat','vlon'])
        d = d.rename({'clon':'lon', 'clat':'lat'})
    return d