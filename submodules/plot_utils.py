import numpy as np
import matplotlib.pyplot as plt
import xarray as xr
from PIL import Image, ImageDraw


def center_around_zero(array):
    # this is a hotfix for colorbars centered around zero
    # it replaces the highest or lowest value in array to make abs(min) == abs(max)
    array_flat = np.ravel(array)
    min_index, min_value = np.argmin(array), array_flat[np.argmin(array)]
    max_index, max_value = np.argmax(array), array_flat[np.argmax(array)]
    if np.abs(min_value) > np.abs(max_value):
        array_flat[max_index] = np.abs(min_value)
    else:
        array_flat[min_index] = - max_value
    array = np.reshape(array_flat, array.shape)
    return array

def remove_spines(ax, borders=['top','bottom','left','right']):    
    for border in borders:
        ax.spines[border].set_visible(False)
    return ax
          
def remove_ticks(ax, borders=['left','right','bottom']):
    left, right, bottom, labelleft, labelbottom = True, True, True, True, True
    if 'left'   in borders: left = False; labelleft = False
    if 'right'  in borders: right = False
    if 'bottom' in borders: bottom = False; labelbottom = False
    ax.tick_params(left=left, right=right, bottom=bottom, 
                   labelleft=labelleft, labelbottom=labelbottom)
    return ax

def bipolar_vminmax(x):
    upper = np.abs(np.nanmax(x.values))
    lower = np.abs(np.nanmin(x.values))
    both  = np.max((upper, lower))
    return (-both, both)

def icon2datetime(icon_dates):
    """
    From: esm_analysis lib
    Convert datetime objects in icon format to python datetime objects.
    :param icon_dates: collection
    :return dates:  pd.DatetimeIndex
    icon_dates i.e
        time = icon2datetime([20011201.5])
    """
    try:
        icon_dates = icon_dates.values
    except AttributeError:
        pass

    try:
        icon_dates = icon_dates[:]
    except TypeError:
        icon_dates = np.array([icon_dates])

    def _convert(icon_date):
        frac_day, date = np.modf(icon_date)
        frac_day *= 60 ** 2 * 24
        date = str(int(date))
        date_str = datetime.datetime.strptime(date, '%Y%m%d')
        td = datetime.timedelta(seconds=int(frac_day.round(0)))
        return date_str + td

    conv = np.vectorize(_convert)
    try:
        out = conv(icon_dates)
    except TypeError:
        out = icon_dates
    if len(out) == 1:
        return pd.DatetimeIndex(out)[0]
    return pd.DatetimeIndex(out)


def month2int(month):
    months = {'january':1,   'february':2, 'march':3,     'april':4,
              'may':5,       'june':6,     'july':7,      'august':8,
              'september':9, 'october':10, 'november':11, 'december':12}
    try:
        return months[month.lower()]
    except:
        raise KeyError(f"Invalid month name given: {month}")
        
def int2month(i):
    months = {'January':1,   'February':2, 'March':3,     'April':4,
              'May':5,       'June':6,     'July':7,      'August':8,
              'September':9, 'October':10, 'November':11, 'December':12}
    try:
        return list(months)[i-1]
    except:
        raise KeyError(f"Invalid month index given: {i}")
        
        
def gif_from_image_list(file_list, save_name=False, delete_frames=False):
    if not save_name: f'generated_clip_{len(file_list)}-frames.gif'
    # https://pillow.readthedocs.io/en/stable/handbook/image-file-formats.html#gif
    img, *imgs = [Image.open(f) for f in file_list]
    img.save(fp=save_name, format='GIF', append_images=imgs,
             save_all=True, duration=len(file_list), loop=0)
    if delete_frames:
        for file in file_list: os.remove(file)
    return save_name


# Lukas's ffmpeg version
def image2mpeg(glob, outfile, framerate=12, resolution='1920x1080'):
    """Combine image files to a video using ``ffmpeg``.
    Notes:
        The function is tested for ``ffmpeg`` versions 2.8.6 and 3.2.2.
    Parameters:
        glob (str): Glob pattern for input files.
        outfile (str): Path to output file.
            The file fileformat is determined by the extension.
        framerate (int or str): Number of frames per second.
        resolution (str or tuple): Video resolution given in width and height
            (``"WxH"`` or ``(W, H)``).
    Raises:
        Exception: The function raises an exception if the
            underlying ``ffmpeg`` process returns a non-zero exit code.
    Example:
        >>> image2mpeg('foo_*.png', 'foo.mp4')
    """
    if not shutil.which('ffmpeg'):
        raise Exception('``ffmpeg`` not found.')

    # If video resolution is given as tuple, convert it into string format
    # to directly pass it to ffmpeg later.
    if isinstance(resolution, tuple) and len(resolution) == 2:
        resolution = '{width}x{height}'.format(width=resolution[0],
                                               height=resolution[1])

    p = subprocess.run(
        ['ffmpeg',
         '-framerate', str(framerate),
         '-pattern_type', 'glob', '-i', glob,
         '-s:v', resolution,
         '-c:v', 'libx264',
         '-profile:v', 'high',
         '-crf', '20',
         '-pix_fmt', 'yuv420p',
         '-y', outfile
         ],
        stdout=subprocess.PIPE,
        stderr=subprocess.PIPE,
        universal_newlines=True
        )