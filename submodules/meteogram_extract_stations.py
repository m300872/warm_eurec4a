#!/usr/bin/env python
# -*- coding: utf-8 -*-

# Author: Hauke Schulz
# taken from:
# https://gitlab.gwdg.de/hernan.campos/EUREC4A-ICON/-/blob/levante_4k/EUREC4A/postprocessing/

import os
import time
import netCDF4
import functools
import datetime
import numpy as np
import pandas as pd
from collections import OrderedDict, namedtuple
from contextlib import contextmanager


DEPTH_METADATA = OrderedDict(
    [
        ("long_name", "depth_below_land"),
        ("units", "m"),
        ("positive", "down"),
        ("axis", "Z"),
    ]
)

HEIGHT_METADATA = OrderedDict(
    [
        ("standard_name", "height"),
        ("long_name", "height"),
        ("units", "m"),
        ("positive", "up"),
        ("axis", "Z"),
    ]
)

_height_meta = namedtuple("HeightMeta", "name value")
HEIGHT_LEVEL_MAP = {
    151: _height_meta('height', HEIGHT_METADATA),
    150: _height_meta('height_2', HEIGHT_METADATA),
    50: _height_meta('height', HEIGHT_METADATA),
    50: _height_meta('height', HEIGHT_METADATA),
    51: _height_meta('height', HEIGHT_METADATA),
    51: _height_meta('height_2', HEIGHT_METADATA),
    9: _height_meta('depth', DEPTH_METADATA),
    8: _height_meta('depth_2', DEPTH_METADATA),
}


@contextmanager
def timeit(msg='', verbose=True):
    st = time.time()
    yield
    elapsed = "{:.2f} sec".format(time.time() - st)
    if verbose: print("Elasped ({}) {}".format(elapsed, msg))


def memoize(func):
    memo = {}

    @functools.wraps(func)
    def wrapper(*args, **kwargs):
        key = (func.__name__, args, frozenset(kwargs.items()))
        if key in memo:
            # print('fetching from cache')
            return memo[key]
        else:
            rv = func(*args, **kwargs)
            memo[key] = rv
            return rv
    return wrapper


class metabase(object):
    def __init__(self, iterable, key_field):
        self.data = list(iterable)
        self.key_field = key_field

    def index(self, name):
        for _index, _name in enumerate(self):
            if name == _name:
                break
        else:
            raise ValueError("{!r} is not in the list. Choose from:{}".format(name,self))
        return _index

    def __iter__(self):
        field = self.key_field
        for item in self.data:
            yield item[field]

    def __getitem__(self, name):
        if isinstance(name, str):
            index = self.index(name)
            data = self[index]
        elif isinstance(name, (tuple, list, dict)):
            data = [self[i] for i in name]
        else:
            data = self.data[name]
        return data

    def __repr__(self):
        return "{!r}({!r})".format(type(self).__name__, list(self))


def _handle_data(data):
    "handles unicode/bytes array"
    data = np.array(data)
    typename = data.dtype.type.__name__
    if typename == 'string_':
        data = [''.join(row) for row in data]
    if typename == 'bytes_':
        data = data.astype('U')
        data = [''.join(row) for row in data]
    return data


@memoize
def station_metadata(ncfile):

    class StationMetadata(metabase):

        def to_string(self, name):
            _data = self[name]
            s = ["{0}={1}".format(*item) for item in _data.items()]
            s = "\n".join(s)
            return s

    nc = netCDF4.Dataset(ncfile)
    snames = [s for s in nc.variables if s.startswith('station')]
    # data = [[b''.join(o).decode('ascii') if o.dtype.kind == 'S' else o
    #          for o in nc.variables[s]] for s in snames]
    data = [_handle_data(nc.variables[s][:]) for s in snames]
    data = [OrderedDict(zip(snames, d)) for d in zip(*data)]
    nc.close()
    return StationMetadata(data, 'station_name')


@memoize
def profile_variables_metadata(ncfile):

    class ProfileVariablesMetadata(metabase):
        pass

    nc = netCDF4.Dataset(ncfile)
    vnames = [s for s in nc.variables if s.startswith('var_')]
    try:
       vnames.remove('var_levels')
    except ValueError:
       pass
    #try:
    #   vnames.remove('var_nlevs')
    #except ValueError:
    #   pass
    _vnames = [v.replace('var_', '') for v in vnames]
    # data = [[b''.join(o).decode('ascii') if o.dtype.kind == 'S' else o
    #          for o in nc.variables[s]] for s in vnames]
    data = [_handle_data(nc.variables[s][:]) for s in vnames]
    data = [OrderedDict(zip(_vnames, d)) for d in zip(*data)]
    nc.close()
    return ProfileVariablesMetadata(data, 'name')


@memoize
def surface_variables_metadata(ncfile):

    class SurfaceVariablesMetadata(metabase):
        pass

    nc = netCDF4.Dataset(ncfile)
    vnames = [s for s in nc.variables if s.startswith('sfcvar_')]
    _vnames = [v.replace('sfcvar_', '') for v in vnames]
    _vnames.append('comment')
    # data = [[b''.join(o).decode('ascii') if o.dtype.kind == 'S' else o
    #          for o in nc.variables[s]] for s in vnames]
    data = [_handle_data(nc.variables[s][:]) for s in vnames]
    # in the absence of surface variables `data[0]` would raise Index Error
    # To avoid it we append extra_entry entry to `data` only if data exists
    if data:
        extra_entry = ['surface variable'] * len(data[0])
        data.append(extra_entry)
    else:
        print("WARNING: surface variables are missing")
    data = [OrderedDict(zip(_vnames, d)) for d in zip(*data)]
    nc.close()
    return SurfaceVariablesMetadata(data, 'name')


def time_meta(ncfile):
    with netCDF4.Dataset(ncfile) as nc:
        d = nc.variables['date'][0]
    d = np.array(d)
    d = b''.join(d).decode('utf-8')
    d = datetime.datetime.strptime(d, '%Y%m%dT%H%M%SZ')
    tmeta = OrderedDict()
    tmeta['standard_name'] = "time"
    tmeta['long_name'] = "time"
    tmeta['units'] = "seconds since {:%Y-%m-%d %H:%M:%S}".format(d)
    tmeta['calendar'] = "proleptic_gregorian"
    tmeta['axis'] = "T"
    return tmeta


def create_ncfile(infile, outfile, station, variables=None, verbose=False):
    if verbose: print("INFO: reading file {}".format(infile))
    if verbose: print("INFO: Gathering metadata")
    station_meta = station_metadata(infile)
    pv_meta = profile_variables_metadata(infile)
    sv_meta = surface_variables_metadata(infile)

    STATION_INDEX = station_meta.index(station)
    if variables is None:
        pvars = list(pv_meta)
        svars = list(sv_meta)
    else:
        pvars = [v for v in pv_meta if v in variables]
        svars = [v for v in sv_meta if v in variables]
    
    heights = sorted(set([pv_meta[p]['nlevs'] for p in pvars]), reverse=True)

    inc = netCDF4.Dataset(infile)
    if len(inc.dimensions['time']) == 0:
        if verbose: print("INFO: time dimension is 0. Skip")
        return
    onc = netCDF4.Dataset(outfile, "w", format=inc.data_model)

    if verbose: print("INFO: creating dimentions")
    onc.createDimension('stringlen', 128)
    for _height in heights:
        h_name = HEIGHT_LEVEL_MAP[_height].name
        onc.createDimension(h_name, _height)
    onc.createDimension('time', None)

    if verbose: print("INFO: creating variables")
    time_step = onc.createVariable('time_step', 'i4', ('time',))
    _time = onc.createVariable('time', 'i4', ('time',))
    _date = onc.createVariable('date', 'S1', ('time', 'stringlen',))
    for _height in heights:
        hname = HEIGHT_LEVEL_MAP[_height].name
        onc.createVariable(hname, 'f8', (hname,))
    for pv in pvars:
        # import pdb; pdb.set_trace()
        nlevs = pv_meta[pv]['nlevs']
        hname = HEIGHT_LEVEL_MAP[nlevs].name
        onc.createVariable(pv, 'f8', ('time', hname))
    for sv in svars:
        onc.createVariable(sv, 'f8', ('time'))

    if verbose: print("INFO: setting meta data")
    time_step.setncattr('description', "time steps")
    _time_meta = time_meta(infile)
    _time.setncatts(_time_meta)
    _date.setncattr('description', "Sample dates")
    for _height in heights:
        h_name, h_value = HEIGHT_LEVEL_MAP[_height]
        hobj = onc.variables[h_name]
        hobj.setncatts(h_value)
    for pv in pvars:
        vmeta = pv_meta[pv]
        if 'unit' in vmeta.keys():
            vmeta['units'] = vmeta.pop('unit')
        vobj = onc.variables[pv]
        vobj.setncatts(vmeta)
    for sv in svars:
        smeta = sv_meta[sv]
        # Make unit attribute CF conform
        if 'unit' in smeta.keys():
            smeta['units'] = smeta.pop('unit')
        vobj = onc.variables[sv]
        vobj.setncatts(smeta)
    inc_meta = inc.__dict__.copy()
    inc_meta['station'] = station_meta.to_string(station)
    onc.setncatts(inc_meta)

    if verbose: print("INFO: copying data")
    with timeit('reading time_step', verbose=verbose):
        time_step_data = inc.variables['time_step'][:]
    with timeit('writing time_step', verbose=verbose):
        time_step[:] = time_step_data[:]
    # with timeit('writing time'):
    #     _time[:] = (time_step_data - time_step_data[0])[:]*3
    with timeit('reading date', verbose=verbose):
        date_data = inc.variables['date'][:]
    with timeit('writing date', verbose=verbose):
        _date[:] = date_data[:]
    with timeit('writing time', verbose=verbose):
        d = [b''.join(np.array(d)).decode('utf-8') for d in date_data]
        d = netCDF4.date2num(pd.to_datetime(d).to_pydatetime(),
                             _time_meta['units'])
        if sum(d - d.astype(int)) == 0:
            d = d.astype(int)
        _time[:] = d[:]
    with timeit('reading heights', verbose=verbose):
        heights_data = inc.variables['heights'][:]
    with timeit('writing heights', verbose=verbose):
        for _height in heights:
            h_name = HEIGHT_LEVEL_MAP[_height].name
            hobj = onc.variables[h_name]
            hindex = None
            for pv in pvars:
                if _height == pv_meta[pv]['nlevs']:
                    hindex = pv_meta.index(pv)
                    break
            h_data = heights_data[:, hindex, STATION_INDEX]
            h_data = h_data[:_height]
            hobj[:] = h_data[:]
    if pvars:
        with timeit('reading profile variables', verbose=verbose):
            pv_data = inc.variables['values'][:]
        with timeit('writing profile variables', verbose=verbose):
            for pv in pvars:
                pv_index = pv_meta.index(pv)
                pv_levels = pv_meta[pv]['nlevs']
                _data = pv_data[:, :pv_levels, pv_index, STATION_INDEX]
                vobj = onc.variables[pv]
                vobj[:, :] = _data[:]
    if svars:
        with timeit('reading surface variables', verbose=verbose):
            sv_data = inc.variables['sfcvalues'][:]
        with timeit('writing surface variables', verbose=verbose):
            for sv in svars:
                sv_index = sv_meta.index(sv)
                _data = sv_data[:, sv_index, STATION_INDEX]
                sobj = onc.variables[sv]
                sobj[:] = _data[:]
    onc.sync()
    onc.close()
    inc.close()
    if verbose: print("INFO: DONE creating file {}".format(outfile))
    return outfile


def cmdline_interface():

    import argparse
    import tqdm

    parser = argparse.ArgumentParser(description=__doc__)
    parser.add_argument(
        "station",
        type=str,
        help="station name.")
    parser.add_argument(
        "filename",
        type=str,
        nargs="+",
        help="input 1d file name(s).")
    parser.add_argument(
        "-d", "--dest",
        type=str,
        nargs="?",
        dest="destdir",
        default="./",
        help="destination directory for output file. (default: pwd)")
    parser.add_argument(
        "-v", "--var",
        type=str,
        nargs="?",
        action="append",
        dest="vname",
        help="variable name. If provided, outfile contains only this variable.")

    args = parser.parse_args()

    filenames = args.filename
    station = args.station
    destdir = os.path.abspath(args.destdir)
    if not os.path.exists(destdir):
        os.makedirs(destdir)
    variables = args.vname
    for input_file in tqdm.tqdm(filenames):
      print(input_file)
      filename_no_ext, ext = os.path.splitext(input_file)
      _basename = os.path.basename(filename_no_ext)
      suffix = station + ext
      if variables and len(variables) == 1:
          outfile = _basename + "_" + variables[0] + "_" + suffix
      else:
          outfile = _basename + "_" + suffix
      outfile = os.path.join(destdir, outfile)
      with timeit("Total Time"):
        try:
          create_ncfile(input_file, outfile, station, variables, verbose=True)
        except ValueError:
          print(f'Issue with {input_file}. Skip.')
    return


if __name__ == '__main__':
    cmdline_interface()
