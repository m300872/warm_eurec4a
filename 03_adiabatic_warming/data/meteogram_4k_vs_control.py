import sys
import os
module_path = '/home/m/m300872/warm_eurec4a/submodules/'
if not module_path in sys.path:
    sys.path.append(module_path)
import file_handling as fh

import matplotlib.pyplot as plt
import cartopy.crs as ccrs
import xarray as xr
from matplotlib.patches import Rectangle, Circle
from matplotlib.collections import PatchCollection
import cartopy.feature as cfeature
import numpy as np
import meteogram_extract_stations as mes


def get_path(experiment):
    # if   experiment == 'warming': return '/work/mh1126/m300872/eureca_icon/EUREC4A/experiments/4kadiabat'
    if   experiment == 'warming': return '/work/mh1126/m300872/eureca_icon/EUREC4A/experiments/4kadiabat2'
    elif experiment == 'rerun'  : return '/work/mh1126/m300872/eureca_icon/EUREC4A/experiments/r02EUREC4A'

def meteogram(experiment_path=get_path('warming'), station='BCO', domain='DOM01', rm_temp=False):
    # mes.create_ncfile(infile, outfile, station, variables=None, verbose=False)
    ifile = [os.path.join(experiment_path,f) for f in os.listdir(experiment_path) if 'Meteo' in f and domain in f][0]
    ofile = f'/scratch/m/m300872/temp/meteo_4kadiabat_{station}.nc'
    mes.create_ncfile(ifile,ofile,station)
    dataset = xr.open_dataset(ofile)
    if rm_temp: os.remove(ofile)
    return dataset

def meteogram_print_vars(meteogram=meteogram()):
    for var in list(meteogram):
        try: print(var.ljust(8), meteogram[var].long_name.ljust(16))
        except: pass

def rectangle_anker(grid):
    lonlim = [np.rad2deg(l) for l in (np.min(grid.clon.values), np.max(grid.clon.values))]
    latlim = [np.rad2deg(l) for l in (np.min(grid.clat.values), np.max(grid.clat.values))]
    anker  = (lonlim[0], latlim[0]) # Rectangle demands coordinates for lower left corner + height and width
    extent = (lonlim[1] - lonlim[0], latlim[1] - latlim[0])
    return anker, extent

def add_map_view(ax, map_projection, latlon_limits, colored_map=False, gridlines=True):
    # decorations
    ax.coastlines()
    if colored_map:
        # (https://techoverflow.net/2021/04/25/how-to-add-colored-background-to-cartopy-map/)
        ax.stock_img()
    else:
        # or just color the land:
        ax.add_feature(cfeature.LAND, facecolor='black', alpha=0.4)
    ax.set_extent(latlon_limits, ccrs.PlateCarree())
    if gridlines:
        ax.gridlines(draw_labels=True, crs=map_projection)

def add_rectangles(ax, rectangles):
    # Create patch collection with specified colour/alpha, inside the loop, because it can not be reused
    pc = PatchCollection(rectangles, facecolor='none', alpha=1, edgecolor='blue', linewidth=2, zorder=2)    
    ax.add_collection(pc)
    
def eureca_domain_rectangles(domains=['DOM01','DOM02'],):
    # get extents of grids as rectangles
    rectangles = list()
    for dom in ['DOM01','DOM02']:
        grid = xr.open_zarr(f"https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/grids/EUREC4A_PR1250m_{dom}.zarr")
        anker, extent = rectangle_anker(grid)
        rectangles.append(Rectangle(anker, *extent))
    return rectangles
    
def plot_grid_rectangles(latlon_limit_list):
    projection = ccrs.PlateCarree(central_longitude=0)
    fig = plt.figure(figsize=[20, 10])
    axs = list()

    for i, latlon_limit in enumerate(latlon_limit_list): 
        ax = fig.add_subplot(1, len(latlon_limit_list)+1, i+1, projection=projection)
        add_map_view(ax, projection, latlon_limit)
        add_rectangles(ax, eureca_domain_rectangles())
        axs.append(ax)

    return fig, axs

def figure_meteogram_locations():
    experiment_path=get_path('warming')
    domain='DOM01'
    ifile = [os.path.join(experiment_path,f) for f in os.listdir(experiment_path) if 'Meteo' in f and domain in f][0]
    dataset = xr.open_dataset(ifile)

    fig, axs = plot_grid_rectangles([(-62,-44,+6,+18)])
    ax = axs[0]
    
    for i in range(7):
        if dataset['station_name'].values[i] == b'c_center' : xy_center = (dataset['station_lon'].values[i], dataset['station_lat'].values[i])
        if dataset['station_name'].values[i] == b'c_south'  : xy_south  = (dataset['station_lon'].values[i], dataset['station_lat'].values[i])
    radius = np.abs(xy_center[1] - xy_south[1])
    circle = Circle(xy_center, radius, facecolor='none', alpha=1, edgecolor='blue', linewidth=0.8, ls=':', label='Halo circle')
    ax.add_patch(circle)

    for dom in ['DOM01','DOM02']:
        grid = xr.open_zarr(f"https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/grids/EUREC4A_PR1250m_{dom}.zarr")
        anker, extent = rectangle_anker(grid)
        ax.annotate(dom, (anker[0], anker[1]+extent[1]), xytext=(-2,5), textcoords='offset pixels')

    for i in range(7):
        xy = (dataset['station_lon'].values[i], dataset['station_lat'].values[i])
        name = dataset['station_name'].values[i].decode("utf-8")
        if name[0:2] == 'c_' : name = name[2::]
        if i == 0: label='Meteogram stations'
        else: label=None
        ax.scatter(*xy, color='blue', s=30, marker='+', label=label)
        ax.annotate(name, xy, xytext=(-2,5), textcoords='offset pixels')
    
    ax.set_title('Location of meteogram stations')
    return fig, axs


from mpl_toolkits.axes_grid1 import make_axes_locatable
import matplotlib.pyplot as plt

def plot_meteogram_timeseries_profile(fig, ax, array, cmap = 'bwr', var_info=False):
    vmax = max([-np.nanmin(array), np.nanmax(array)])
    im = ax.imshow(array,cmap=cmap, vmin=-vmax, vmax=vmax)
    ax.set_xlabel('time / min')
    ax.set_ylabel('model level')
    ax.set_xlim(0,array.shape[1])

    divider = make_axes_locatable(ax)
    cax = divider.append_axes('right', size='5%', pad=0.05)
    label = 'Difference to $t_0$'
    if var_info: label=f'Difference in {var_info["name"]} to $t_0$ / {var_info["unit"]}' 
    plt.colorbar(im, cax=cax, orientation='vertical', label=label)
    return fig, ax

def figure_meteogram_single(meteogram, varname):
    y = meteogram[varname].values
    y_mean  = np.asarray([y.mean(axis=0) for i in range(y.shape[0])])
    y_diff = y - y_mean
    y_first = np.asarray([y[0] for i in range(y.shape[0])])
    y_diff = y - y_first
    array = y_diff.transpose()

    fig = plt.figure(figsize=(12, 6))
    ax = fig.add_subplot(111)
    var_info = {'name' : meteogram[varname].long_name, 'unit' : meteogram[varname].units}
    fig, ax = plot_meteogram_timeseries_profile(fig, ax, array, var_info=var_info)
    return fig, ax

def figure_meteogram_quad(meteogram, varname):
    fig = plt.figure(figsize=(20, 12))
    for i, e in enumerate(varname):
        y = meteogram[e].values
        y_mean  = np.asarray([y.mean(axis=0) for i in range(y.shape[0])])
        y_diff = y - y_mean
        y_first = np.asarray([y[0] for i in range(y.shape[0])])
        y_diff = y - y_first
        array = y_diff.transpose()
        
        ax = fig.add_subplot(221 + i)
        var_info = {'name' : meteogram[e].long_name, 'unit' : meteogram[e].units}
        fig, ax = plot_meteogram_timeseries_profile(fig, ax, array, var_info=var_info)
    return fig, ax


def station_line(lat_range, lon_range, n, name_prefix=''):
    stations = list()
    for i in range(n):
        station = dict()
        station['lat']  = np.linspace(*lat_range, n)[i]
        station['lon']  = np.linspace(*lon_range, n)[i]
        station['name'] = f'{name_prefix}{str(i).rjust(2,"0")}'
        stations.append(station)
    return stations

def fig_proposed_meteogram_stations(label='additional stations'):
    vertical   = station_line((7.5, 16.5), (-52.5, -52.5), 13, 'v_')
    horizontal = station_line((12,12), (-60.0, -45.0), 13, 'h_')

    fig, axs = figure_meteogram_locations()
    ax = axs[0]
    for stat in vertical + horizontal:
        if stat == vertical[0]: effectivelabel=label
        else: effectivelabel=None
        ax.scatter(stat['lon'], stat['lat'], color='red', s=100, marker='.', label=effectivelabel)
    for axis in vertical, horizontal:
        ax.plot([axis[0]['lon'], axis[-1]['lon']], [axis[0]['lat'], axis[-1]['lat']], color='red', linestyle=':', lw=2)
    ax.legend(facecolor="white")
    return fig, axs

def extract_meteogram(meteogram, varname, length=False, limit=False):
    y = meteogram[varname].values
    if limit: y = limit_values(y,limit)
    if length: y = y[0:length][...]
    if not length: length = y.shape[0]
    y_first = np.asarray([y[0] for i in range(length)])
    y_diff = y - y_first
    # array = center_around_zero(y_diff)
    array = y_diff.transpose()
    return array

def plot_meteogram_timeseries_profile(fig, ax, array, cmap = 'bwr', var_info=False, vmax=False, cbar=False):
    if not vmax: vmax = get_vmax(array)
    im = ax.imshow(array,cmap=cmap, vmin=-vmax, vmax=vmax)
    ax.set_xlabel('time / min')
    ax.set_ylabel('model level')
    ax.set_xlim(0,array.shape[1])
    if cbar:
        divider = make_axes_locatable(ax)
        cax = divider.append_axes('right', size='5%', pad=0.05)
        label = 'Difference to $t_0$'
        if var_info: label=f'Difference in {var_info["name"]} to $t_0$ / {var_info["unit"]}' 
        plt.colorbar(im, cax=cax, orientation='vertical', label=label)
    return fig, ax

def fig_warming_vs_control(station='BCO', varname='T', domain='DOM01'):
    fig = plt.figure(figsize=(20, 12))
    ax = fig.add_subplot(221 + 0)
    
    experiment = 'rerun'
    m = meteogram(experiment_path=get_path(experiment), station=station, domain=domain)
    y = extract_meteogram(m, varname, 241)
    var_info = {'name' : m[varname].long_name, 'unit' : m[varname].units}
    plot_meteogram_timeseries_profile(fig, ax, y, var_info=var_info, vmax=6)
    ax.set_title(f'Control')

    ax = fig.add_subplot(221 + 1)
    
    experiment = 'warming'
    m = meteogram(experiment_path=get_path(experiment), station=station, domain=domain)
    y2 = extract_meteogram(m, varname, 241)
    y = y - y2
    y = y2
    var_info = {'name' : m[varname].long_name, 'unit' : m[varname].units}
    plot_meteogram_timeseries_profile(fig, ax, y, var_info=var_info, vmax=6, cbar=True)
    ax.set_title(f'{experiment}, {station}')
    ax.set_title(f'Adiabatic warming (+4)')
    return fig

def make():
    fig = fig_warming_vs_control()
    plt.show()