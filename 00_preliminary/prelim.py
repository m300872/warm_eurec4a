# multi-purpose tools
import numpy as np
import pandas as pd
import intake
import xarray as xr
xr.set_options(keep_attrs=True)
from functools import partial
from function_timer import function_timer

# plotting
import matplotlib.pyplot as plt
from matplotlib.patches import Rectangle
from matplotlib.collections import PatchCollection
import cartopy.crs as ccrs
import cartopy.feature as cfeature
import datashader
from datashader.mpl_ext import dsshow
import datashader.transfer_functions as dstf
import cmocean


def latlon_limits(grid, latname='clat', lonname='clon'):
    lonlim = [np.rad2deg(l) for l in (np.min(grid[lonname].values), np.max(grid[lonname].values))]
    latlim = [np.rad2deg(l) for l in (np.min(grid[latname].values), np.max(grid[latname].values))]
    return tuple(latlim), tuple(lonlim)
    
def rectangle(grid):
    latlim, lonlim = latlon_limits(grid)
    anker  = (lonlim[0], latlim[0]) # Rectangle demands coordinates for lower left corner + height and width
    extent = (lonlim[1] - lonlim[0], latlim[1] - latlim[0])
    return Rectangle(anker, *extent)

def add_map_view(ax, map_projection, latlon_limits, colored_map=False, gridlines=True):
    # decorations
    ax.coastlines()
    if colored_map:
        # (https://techoverflow.net/2021/04/25/how-to-add-colored-background-to-cartopy-map/)
        ax.stock_img()
    else:
        # or just color the land:
        ax.add_feature(cfeature.LAND, facecolor='black', alpha=0.4)
    ax.set_extent(latlon_limits, ccrs.PlateCarree())
    if gridlines:
        ax.gridlines(draw_labels=True, crs=map_projection)

def add_rectangles(ax, rectangles):
    # Create patch collection with specified colour/alpha, inside the loop, because it can not be reused
    pc = PatchCollection(rectangles, facecolor='blue', alpha=0.4, edgecolor=None)    
    ax.add_collection(pc)
    
    
def eureca_grid(domain='DOM01'):
    return xr.open_zarr(f"https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/grids/EUREC4A_PR1250m_{domain}.zarr")
    
def eureca_domain_rectangles(domains=['DOM01','DOM02'],):
    # get extents of grids as rectangles
    rectangles = list()
    for dom in domains:
        rectangles.append(rectangle(eureca_grid(dom)))
    return rectangles
    
def plot_grid_rectangles(map_extends):
    projection = ccrs.PlateCarree(central_longitude=0)
    fig = plt.figure(figsize=[20, 10])
    axs = list()

    for i, latlon_limit in enumerate(map_extends): 
        ax = fig.add_subplot(1, len(map_extends)+1, i+1, projection=projection)
        add_map_view(ax, projection, latlon_limit)
        add_rectangles(ax, eureca_domain_rectangles())
        axs.append(ax)

    return fig, axs




def monsooon_catalog():
    cat_yaml = "/work/bd1154/highresmonsoon/experiments/monsoon2.yaml"
    cat_yaml = "/work/bd1154/highresmonsoon/monsoon2.yaml" #changed 29.10.2022
    return intake.open_catalog(cat_yaml)

def monsoon_dataset_from_catalog(name, type='atm2d'):
    return monsooon_catalog()[name][type].to_dask()

def monsoon_dataset(name='luk1000', grid=False, coarsen=False, type='atm2d'):
    dataset = monsoon_dataset_from_catalog(name, type=type)
    if grid:
        dataset = add_grid(dataset)
    if coarsen:
        dataset = mean_coarsen(dataset, coarsen)
    return dataset

def monsoon_grids():
    cat = monsooon_catalog()
    return cat['grids']

def lonlatlim(grid):
    if grid in ['DOM01','DOM02']: 
        grid = xr.open_zarr(f"https://swift.dkrz.de/v1/dkrz_948e7d4bbfbb445fbff5315fc433e36a/grids/EUREC4A_PR1250m_{grid}.zarr")
    return lonlatlim_fromgrid(grid)

def lonlat_slice(grid):
    lon,lat = lonlatlim_fromgrid(grid)
    slices = dict()
    slices['clat'] = slice(*lat)
    slices['clon'] = slice(*lon)
    return slices

def lonlatlim_fromgrid(grid):
    lonlim = [np.rad2deg(l) for l in (np.min(grid.clon.values), np.max(grid.clon.values))]
    latlim = [np.rad2deg(l) for l in (np.min(grid.clat.values), np.max(grid.clat.values))]
    return lonlim, latlim

# old
def matching_grid(dataset):
    ncell = len(dataset.cell.values)
    for g in list(monsoon_grids()):
        if ncell == len( monsoon_grids()[g].to_dask().cell.values):
            return monsoon_grids()[g].to_dask()
# replacement:
def matching_grid(dataset):
    return monsooon_catalog().grids[dataset.uuidOfHGrid].to_dask()


def add_grid(dataset, drop_vars=True, drop_dims=True):
    grid = matching_grid(dataset)
    gridvars = list(grid)
    d = xr.merge([dataset,grid])
    if drop_vars:
        d = d.drop_vars(gridvars)
    if drop_dims:
        d = d.sel(edge=0, drop=True)
        d = d.sel(vertex=0, drop=True)
    return d

def mean_coarsen(dataset, exp, base=4):
    ''' 
    the base has been chosen for the icon native grid:
    4 triangles can be joined to one. The grid ordering makes 
    this operation possible. 
    See: https://easy.gems.dkrz.de/Processing/playing_with_triangles/subsampling_and_averaging.html
    '''
    return dataset.coarsen(cell=base**exp).mean()

def cut_latlon_box(dataset, latlon_tuple, latname='clat', lonname='clon'):
    (latmin, latmax), (lonmin, lonmax) = latlon_tuple
    cond = (
        (dataset[latname] > np.deg2rad(latmin)) &
        (dataset[latname] < np.deg2rad(latmax)) &
        (dataset[lonname] > np.deg2rad(lonmin)) &
        (dataset[lonname] < np.deg2rad(lonmax))
    )
    return dataset.isel(cell = cond.values)



def plot_dual_globe(datasets, grid, varname, labels=False,
                    timeindex=0, viewpoint=(0,0,1e8), vminmax=(0,600), 
                    cmap=cmocean.cm.thermal, figsize=(15,10)):
    if labels: assert(len(labels) == len(datasets))
    # unpacking
    central_longitude, central_latitude, satellite_height = viewpoint
    vmin, vmax = vminmax
    # projection
    projection = ccrs.NearsidePerspective(central_longitude=central_longitude, central_latitude=central_latitude, satellite_height=satellite_height)
    coords = projection.transform_points(
        ccrs.Geodetic(),
        np.rad2deg(grid.clon),
        np.rad2deg(grid.clat)
        )
    fig, axs = plt.subplots(1,len(datasets),figsize=figsize,subplot_kw={"projection": projection})
    fig.canvas.draw_idle() # its a bug fix...
    for i,d in enumerate(datasets):
        ax = axs[i]
        if labels: ax.set_title(labels[i])
        variable = d[varname].isel(time=timeindex)
        ax.add_feature(cfeature.COASTLINE, linewidth=0.8)
        artist = dsshow(
            pd.DataFrame({
                "val": variable.values,
                "x": coords[:, 0],
                "y": coords[:, 1],
            }),
            datashader.Point('x', 'y'),
            datashader.mean('val'),
            vmin=vmin, vmax=vmax, cmap=cmap,
            ax=ax,
        )
        
        
        
def draw_domain(ax, variable, coords, vminmax=False, cmap='plasma', spread=None, coastline=False):
    if vminmax:
        vmin, vmax = vminmax
    else:
        vmin = np.min(variable.values); vmax = np.max(variable.values)
    if spread == True:
        spread = partial(dstf.spread, px=2)
    if coastline:
        ax.add_feature(cfeature.COASTLINE, linewidth=0.8)
        
    artist = dsshow(
        pd.DataFrame({
            "val": variable.values,
            "x": coords[:, 0],
            "y": coords[:, 1],
        }),
        datashader.Point('x', 'y'),
        datashader.mean('val'),
        vmin=vmin, vmax=vmax, cmap=cmap,
        ax=ax,
        # https://github.com/holoviz/datashader/issues/1068
        # shade_hook=partial(dstf.dynspread, threshold=0.50, how='over')
        shade_hook=spread
    )
    return artist
    
def proj2coords(grid, projection):
    coords = projection.transform_points(
        ccrs.Geodetic(),
        np.rad2deg(grid.clon),
        np.rad2deg(grid.clat))
    return coords

def plot_single_domain(dataarray, projection, label=False, vminmax=False, figsize=(15,10), timeindex=500, cmap=cmocean.cm.thermal, colorbar=False):
    fig, ax = plt.subplots(figsize=figsize,subplot_kw={"projection": projection})
    fig.canvas.draw_idle() # its a bug fix...
    coords = proj2coords(dataarray,projection)
    if label: ax.set_title(f"{label}")
    variable = dataarray.isel(time=timeindex)
    artist = draw_domain(ax, variable, coords, vminmax=vminmax, cmap=cmap, spread=True)
    if colorbar: fig.colorbar(artist, label=colorbar)
    return fig, ax


def plot_multi_domain(dataarrays, projection, labels=False, vminmax=False, figsize=(15,10), timeindex=500, cmap=cmocean.cm.thermal):
    fig, axs = plt.subplots(1,len(dataarrays), figsize=figsize, subplot_kw={"projection": projection})
    fig.canvas.draw_idle() # its a bug fix...
    for i,d in enumerate(dataarrays):
        coords = proj2coords(d,projection)
        ax = axs[i]
        variable = d.isel(time=timeindex)
        if labels: ax.set_title(f"{labels[i]}")
        artist = draw_domain(ax, variable, coords, vminmax=vminmax, cmap=cmap, spread=True)
        # fig.colorbar(artist, label=f"{variable.long_name} / {variable.units}")
    return fig, axs
    
def plot_domain(data, projection, label=False, figsize=(15,10), timeindex=500, vminmax=False, cmap=cmocean.cm.thermal):
    if type(data) == xr.core.dataarray.DataArray : 
        return plot_single_domain(data, projection, label=label, vminmax=vminmax, figsize=figsize, timeindex=timeindex, cmap=cmap)
    if type(data) in (type(tuple()), type(list())) : 
        return plot_multi_domain(data, projection, labels=label, vminmax=vminmax ,figsize=figsize, timeindex=timeindex, cmap=cmap)
    

def plot_monsoon_warming_comparison_domain(domain_limits, return_axs=False):
    varname = 'ts'
    timeindex = 500
    viewpoint=(-50, 15, 2e6)
    central_longitude, central_latitude, satellite_height = viewpoint
    vminmax = (290, 305) # degree Kelvin

    ctrl = cut_latlon_box(monsoon_dataset('luk1000', grid=True), domain_limits)[varname]
    plus = cut_latlon_box(monsoon_dataset('luk1010', grid=True), domain_limits)[varname]

    projection = ccrs.PlateCarree(central_longitude=central_longitude)
    coords = proj2coords(ctrl,projection)

    data  = [ctrl, plus]
    label = ['ctrl','RCP4.5']

    fig, axs = plot_domain(data, projection, label, timeindex=timeindex, vminmax=vminmax)
    if return_axs: return fig, axs


def plot_delta(delta, timeindex=500):
    projection = ccrs.PlateCarree(central_longitude=-50)
    # to center the colorscale around zero:
    absmax = np.max([np.abs(np.min(delta.isel(time=timeindex).values)),
                     np.abs(np.max(delta.isel(time=timeindex).values))] )
    vminmax = (-absmax, +absmax)
    plot_single_domain(delta, projection, vminmax=vminmax, colorbar='deltaT / K', cmap=cmocean.cm.balance)


    
def delta(varname, simulations=['luk1000','luk1010'], domain='eureca'):
    simname_ctrl, simname_plus = simulations
    if domain == 'eureca': domain_limits = latlon_limits(eureca_grid())
    ctrl = cut_latlon_box(monsoon_dataset(simname_ctrl, grid=True), domain_limits)[varname]
    plus = cut_latlon_box(monsoon_dataset(simname_plus, grid=True), domain_limits)[varname]
    
    offset = plus.time[0].values - ctrl.time[0].values
    plus['time'] = plus.time.values - offset

    blame = ctrl.time.values[np.isin(ctrl.time, plus.time, invert=True)]
    for b in blame:
        ctrl = ctrl.where(ctrl.time != b, drop=True)
        
    delta_series = plus - ctrl
    delta_series.attrs['name'] = f"$\Delta$ {delta_series.attrs['name']}"
    return delta_series

def before_date(dataarray, before='may2020'):
    if before == 'may2020': before = np.datetime64('2020-05-01T00:00:00.000000000')
    return dataarray.where(dataarray.time < before, drop=True)

def spatial_mean(dataarray):
    return dataarray.mean(dim='cell')

@function_timer
def april_timeseries(varname, simulations=['luk1000','luk1010'], domain='eureca'):
    return spatial_mean(before_date(delta(varname, simulations=simulations, domain=domain)))

@function_timer
def add_line_plot(ax,timeseries,label=None):
    if not label: label=timeseries.attrs['name']
    timeseries.plot.line(ax=ax, label=label)

def plot_delta_timeseries_april(
    simulations=['luk1000','luk1010'],
    variables = ['ts','tas'],
    # ylim = (-2.0, 2.0),
    # ylabel = "$\Delta$ temperature (warming - ctrl)",
    ylim=None, ylabel=None,
    figsize=(15,10) ):
    
    fig, ax = plt.subplots(figsize=figsize)

    for var in variables:
        timeseries = april_timeseries(var, simulations=simulations)
        add_line_plot(ax, timeseries, f"{timeseries.attrs['name']} / K")
        
    ax.legend()
    if ylim: ax.set_ylim(*ylim)
    if ylabel: ax.set_ylabel(ylabel)
    return fig, ax